package pollub.iist.sem6.ism.lab3a.util.orm.types;

public interface SqlValueParser {
	String fromValue(Object value);
	Object fromString(String string);
}
