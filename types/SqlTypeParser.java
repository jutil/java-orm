package pollub.iist.sem6.ism.lab3a.util.orm.types;

import java.util.HashMap;

public abstract class SqlTypeParser {
	protected HashMap<Class<?>,String>typesMapping;
	protected HashMap<Class<?>,SqlValueParser>valuesMapping;
	
	public SqlTypeParser() {
		typesMapping=new HashMap<>();
		valuesMapping=new HashMap<>();
	}
	
	public final String parse(Class<?>type) {
		if(type==null)throw new IllegalArgumentException("null @type");
		String out=typesMapping.get(type);
		if(out==null)throw new IllegalArgumentException("no mapping for "+type.getName());
		return out;
	}
	public final SqlValueParser getValueParser(Class<?>type) {
		if(type==null)throw new IllegalArgumentException("null @type");
		SqlValueParser out=valuesMapping.get(type);
		if(out==null)throw new IllegalArgumentException("no mapping for "+type.getName());
		return out;
	}
	
	public void register(Class<?>javaType,String sqlType,SqlValueParser parser) {
		register(javaType,sqlType,parser,false);
	}
	public void register(Class<?>javaType,String sqlType,SqlValueParser parser,boolean override) {
		if(javaType==null)throw new IllegalArgumentException("null @type");
		if(parser  ==null)throw new IllegalArgumentException("null @parser");
		if(typesMapping.containsKey(javaType)&&!override)
			throw new IllegalArgumentException(javaType.getName()+" already mapped");
		typesMapping.put(javaType, sqlType);
		valuesMapping.put(javaType, parser);
	}
}
