package pollub.iist.sem6.ism.lab3a.util.orm.types;

import java.util.HashMap;

import pollub.iist.sem6.ism.lab3a.util.orm.core.provider.PredefinedSqlRdbms;
import pollub.iist.sem6.ism.lab3a.util.orm.core.provider.SqlRdbms;
import pollub.iist.sem6.ism.lab3a.util.orm.types.predefined.*;

public class SqlTypeParserRegister {
	private static HashMap<SqlRdbms,SqlTypeParser> rdbmsParsers=new HashMap<>();
	static {
		rdbmsParsers.put(PredefinedSqlRdbms.MSSQL,     new MsSqlTypeParser());
		rdbmsParsers.put(PredefinedSqlRdbms.MySQL,     new MySqlTypeParser());
		rdbmsParsers.put(PredefinedSqlRdbms.Oracle,    new OracleSqlTypeParser());
		rdbmsParsers.put(PredefinedSqlRdbms.PostgreSQL,new PostgreSqlTypeParser());
		rdbmsParsers.put(PredefinedSqlRdbms.SQLite,    new SqLiteTypeParser());
	}
	
	public static final String parse(SqlRdbms rdbms,Class<?>type) {
		SqlTypeParser tp=rdbmsParsers.get(rdbms);
		if(tp==null)throw new IllegalArgumentException("no/null mapping for @rdbms");
		return tp.parse(type);
	}
	
	public static void register(SqlRdbms rdbms,SqlTypeParser parser) {
		register(rdbms,parser,false);
	}

	public static void register(SqlRdbms rdbms, SqlTypeParser parser, boolean override) {
		if(rdbmsParsers.containsKey(rdbms)&&!override)
			throw new IllegalArgumentException("@rdbms already mapped");
		rdbmsParsers.put(rdbms, parser);
	}
	
	public static SqlTypeParser get(SqlRdbms rdbms) {
		SqlTypeParser tp=rdbmsParsers.get(rdbms);
		if(tp==null)throw new IllegalArgumentException("no/null mapping for @rdbms");
		return tp;
	}
}
