package pollub.iist.sem6.ism.lab3a.util.orm.types.predefined;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

import pollub.iist.sem6.ism.lab3a.util.orm.types.SqlTypeParser;
import pollub.iist.sem6.ism.lab3a.util.orm.types.SqlValueParser;

public class PostgreSqlTypeParser extends SqlTypeParser {
	public static int DEF_STRLEN=50;
	
	public PostgreSqlTypeParser() {
		this(DEF_STRLEN);
	}
	public PostgreSqlTypeParser(final int strLen) {
		super();
		typesMapping.put(Boolean.class, "BOOLEAN");
		valuesMapping.put(Boolean.class,new SqlValueParser() {
			@Override
			public String fromValue(Object value) {
				try {
					return value.toString();
				}catch(Exception e) {
					return null;
				}
			}

			@Override
			public Object fromString(String string) {
				try {
					return Boolean.parseBoolean(string);
				}catch(Exception e) {
					return null;
				}				
			}
		});
		typesMapping.put(Integer.class, "INTEGER");
		valuesMapping.put(Integer.class,new SqlValueParser() {
			@Override
			public String fromValue(Object value) {
				try {
					return value.toString();
				}catch(Exception e) {
					return null;
				}
			}

			@Override
			public Object fromString(String string) {
				try {
					return Integer.parseInt(string);
				}catch(Exception e) {
					return null;
				}				
			}
		});
		typesMapping.put(Float.class,   "REAL");
		valuesMapping.put(Float.class,new SqlValueParser() {
			@Override
			public String fromValue(Object value) {
				try {
					return value.toString();
				}catch(Exception e) {
					return null;
				}
			}

			@Override
			public Object fromString(String string) {
				try {
					return Float.parseFloat(string);
				}catch(Exception e) {
					return null;
				}				
			}
		});
		typesMapping.put(Double.class,  "DOUBLE PRECISION");
		valuesMapping.put(Double.class,new SqlValueParser() {
			@Override
			public String fromValue(Object value) {
				try {
					return value.toString();
				}catch(Exception e) {
					return null;
				}
			}

			@Override
			public Object fromString(String string) {
				try {
					return Double.parseDouble(string);
				}catch(Exception e) {
					return null;
				}
			}
		});
		typesMapping.put(String.class,  "VARCHAR("+strLen+")");
		valuesMapping.put(String.class,new SqlValueParser() {
			@Override
			public String fromValue(Object value) {
				String strData=(String)value;
				if(strData.length()>strLen)strData=strData.substring(0, strLen);
				return strData;
			}

			@Override
			public Object fromString(String string) {
				return string;
			}
		});
		typesMapping.put(Serializable.class,  "BYTEA");
		valuesMapping.put(Serializable.class,new SqlValueParser() {
			@Override
			public String fromValue(Object value) {
				try {
					ByteArrayOutputStream baos=new ByteArrayOutputStream();
				    ObjectOutputStream oos = new ObjectOutputStream(baos);
				    oos.writeObject(value);
				    oos.flush();
				    return new String(baos.toByteArray());
				}catch(Exception e) {
					return null;
				}
			}

			@Override
			public Object fromString(String string) {
				try {
					  ByteArrayInputStream bais = new ByteArrayInputStream(string.getBytes());
					  ObjectInputStream ois = new ObjectInputStream(bais);
					  return ois.readObject();
				}catch(Exception e) {
					return null;
				}				
			}
		});
	}
	
	public void setStrLen(int strLen) {
		typesMapping.put(String.class, "VARCHAR("+strLen+")");
	}
}
