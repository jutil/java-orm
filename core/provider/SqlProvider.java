package pollub.iist.sem6.ism.lab3a.util.orm.core.provider;

import pollub.iist.sem6.ism.lab3a.util.orm.core.exception.OrmException;

public interface SqlProvider {
	/*/// query(query) -> result
	// usage:
	//   executes any @query against underlying database handle
	// arguments:
	//   @query   string     query string
	//   $result  object     resulting set of rows or null
	Object query(String query)throws OrmException;*/
	
	/// schema(query) -> void
	// usage:
	//   performs @name schema CRUD using @query
	// arguments:
	//   @name    string     schema name
	//   @query   string     schema CRUD string
	void   schema(String name,String query)throws OrmException;
	/// table(query) -> void
	// usage:
	//   performs @name table CRUD using @query
	// arguments:
	//   @name    string     table name
	//   @query   string     table CRUD string
	void   table(String name,String query)throws OrmException;
	
	/// select(table,query) -> result
	// usage:
	//   performs @query specified select against @table
	// arguments:
	//   @table   string     target table name
	//   @query   string     select query string
	//   $result  object     resulting set of rows or null
	Object select(String table,String query)throws OrmException;
	/// select(columns,from,where,groupBy,orderBy) -> result
	// usage:
	//   performs select with specified parameters on internal database handle
	// arguments:
	//   @columns string[]   list of columns
	//   @from    string     target table
	//   @where   string     pre-fetch condition string
	//   @groupBy string[]   list of columns
	//   @having  string     post-fetch condition string
	//   @orderBy string     output sorting order
	//   @limit   string     result set filter
	//   $result  object     resulting set of rows or null
	Object select(String[]columns,
				  String  from,
				  String  where,
				  String[]groupBy,
				  String  having,
				  String  orderBy,
				  String  limit)throws OrmException;
	
	/// insert(table,values) -> void
	// usage:
	//   performs insert with @values into @table
	// arguments:
	//   @data    entity     appropriately annotated object describing table row
	void   insert(Object data)throws OrmException;
	/// update(table,values,where) -> void
	// usage:
	//   performs update with @values of @where specified rows on @table
	// arguments:
	//   @data    entity     appropriately annotated object describing table row
	void   update(Object data)throws OrmException;
	/// delete(table,where) -> void
	// usage:
	//   performs delete of @where specified rows on @table
	// arguments:
	//   @data    entity     appropriately annotated object describing table row
	void   delete(Object data)throws OrmException;
}
