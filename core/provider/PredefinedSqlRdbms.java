package pollub.iist.sem6.ism.lab3a.util.orm.core.provider;

public enum PredefinedSqlRdbms implements SqlRdbms {
	MySQL,MSSQL,PostgreSQL,Oracle,SQLite;
}
