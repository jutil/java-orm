package pollub.iist.sem6.ism.lab3a.util.orm.core.entity;

import java.lang.reflect.Field;
import java.util.LinkedList;
import java.util.LinkedHashMap;
import java.util.Map.Entry;

import pollub.iist.sem6.ism.lab3a.util.orm.core.annotation.*;
import pollub.iist.sem6.ism.lab3a.util.orm.core.annotation.constraint.*;
import pollub.iist.sem6.ism.lab3a.util.orm.core.annotation.constraint.key.*;
import pollub.iist.sem6.ism.lab3a.util.orm.core.exception.ConstraintViolationException;
import pollub.iist.sem6.ism.lab3a.util.orm.core.exception.EntityRegistrationException;
import pollub.iist.sem6.ism.lab3a.util.orm.core.exception.OrmException;
import pollub.iist.sem6.ism.lab3a.util.orm.query.SqlColumnParam;
import pollub.iist.sem6.ism.lab3a.util.orm.query.SqlConstraintParam;
import pollub.iist.sem6.ism.lab3a.util.orm.query.SqlConstraintType;
import pollub.iist.sem6.ism.lab3a.util.orm.query.SqlQueryBuilder;
import pollub.iist.sem6.ism.lab3a.util.orm.types.SqlTypeParser;

public class SqlDataRegister {
	public static final int ERR_ENTITYREGISTERED=1,
			                ERR_NOENTITY=2,
			                ERR_SQLSYNTAX=4;
	
	private String                     lastException=null;
	private LinkedHashMap<Class<?>,SqlEntity>entityMapping=new LinkedHashMap<>();
	
	
	public  LinkedList<SqlEntity> register(Class<?>entity,SqlQueryBuilder qb,SqlTypeParser tp) throws EntityRegistrationException {
		SqlDataRegister tmp=new SqlDataRegister();
		int regRes=register(entity,qb,tp,tmp);
		if(regRes!=0)throw new EntityRegistrationException(regRes,lastException);
		return sync(tmp,true);
	}
	private int register(Class<?> entity, SqlQueryBuilder qb, SqlTypeParser tp, SqlDataRegister tmp) {
		if(this.contains(entity)||tmp.contains(entity)) {
			lastException="@entity already registered";
			return ERR_ENTITYREGISTERED;
		}
		Entity entityAnnotation=entity.getAnnotation(Entity.class);
		if(entityAnnotation==null) {
			lastException="@entity missing annotation";
			return ERR_NOENTITY;
		}
		SqlEntity entry=new SqlEntity(entity,qb);
		entry.setTable(entityAnnotation.table().length()==0?entity.getSimpleName():entityAnnotation.table());
		
		int res=0;
		for(Field f : entity.getDeclaredFields()) {
			res=parseField(f,entry,qb,tp,tmp);
			if(res!=0)return res;
		}
		try{
			entry.optimize();
		}catch(Exception e) {
			lastException=e.getMessage();
			return ERR_SQLSYNTAX;
		}
		
		tmp.entityMapping.put(entity, entry);
		return res;
	}
	private int parseField(Field f,SqlEntity entry,SqlQueryBuilder qb, SqlTypeParser tp, SqlDataRegister tmp) {
		int res=0;
		Column columnAnnotation=f.getAnnotation(Column.class);
		if(columnAnnotation==null)return 0;
		f.setAccessible(true);
		
		SqlColumn col=new SqlColumn();
		res=parseColumn(f,columnAnnotation,entry,col,qb,tp,tmp);
		if(res!=0)return res;
		if(f.getDeclaredAnnotations().length==1)return 0;
			
		Unique     uniqueAnnotation    =f.getAnnotation(Unique.class);
		Check      checkAnnotation     =f.getAnnotation(Check.class);
		PKey       pkeyAnnotation      =f.getAnnotation(PKey.class);
		FKey       fkeyAnnotation      =f.getAnnotation(FKey.class);
		if(uniqueAnnotation!=null&&pkeyAnnotation==null) {
			res=parseUnique(uniqueAnnotation,entry,col,qb,tp,tmp);
			if(res!=0)return res;
		}
		if(checkAnnotation!=null) {
			res=parseCheck(checkAnnotation,entry,col,qb,tp,tmp);
			if(res!=0)return res;
		}
		if(pkeyAnnotation!=null) {
			if(fkeyAnnotation!=null) {
				lastException="primary key redeclared as foreign key";
				return ERR_SQLSYNTAX;
			}
			res=parsePkey(pkeyAnnotation,entry,col,qb,tp,tmp);
			if(res!=0)return res;
		}
		if(fkeyAnnotation!=null) {
			res=parseFkey(fkeyAnnotation,entry,col,qb,tp,tmp);
			if(res!=0)return res;
		}	
		return 0;
	}
	private int parseColumn(Field f,Column columnAnnotation,SqlEntity entry,SqlColumn col,
			SqlQueryBuilder qb, SqlTypeParser tp, SqlDataRegister tmp) {
		try{
			col.setColName(columnAnnotation.name().length()==0?
			                  f.getName():
				              columnAnnotation.name());
			col.setFieldName(f.getName());
			col.setSqlType(columnAnnotation.type().length()==0?
				              tp.parse(f.getType()):
				              columnAnnotation.type());
			col.setJavaType(f.getType());
			if(!columnAnnotation.nullable())col.addModifier(SqlColumnParam.notNull);
			col.setValueParser(tp.getValueParser(f.getType()));
			entry.addColumn(col);		
		} catch(OrmException e) {
			e.printStackTrace();
			lastException=e.getMessage();
			return ERR_SQLSYNTAX;
		}
		return 0;
	}
	private int parseUnique(Unique uniqueAnnotation,SqlEntity entry,SqlColumn col,
			SqlQueryBuilder qb, SqlTypeParser tp, SqlDataRegister tmp) {
		try{
			SqlConstraint c=new SqlConstraint();
			c.setConstraintName(uniqueAnnotation.name().length()==0?
				                  "uq_"+entry.getTable()+"_"+col.getColName():
				                  uniqueAnnotation.name());
			c.setConstraintType(SqlConstraintType.unique);
			c.addColumn(col);
			entry.addConstraint(c);		
		} catch(OrmException e) {
			e.printStackTrace();
			lastException=e.getMessage();
			return ERR_SQLSYNTAX;
		}
		return 0;
	}
	private int parseCheck(Check checkAnnotation,SqlEntity entry,SqlColumn col,
			SqlQueryBuilder qb, SqlTypeParser tp, SqlDataRegister tmp) {
		try {
			SqlConstraint c=new SqlConstraint();
			c.setConstraintName(checkAnnotation.name().length()==0?
					               "chk_"+entry.getTable():
					               checkAnnotation.name());
			c.setConstraintType(SqlConstraintType.check);
			c.addColumn(col);
			c.addParam(SqlConstraintParam.CHK_COND, checkAnnotation.condition());
			entry.addConstraint(c);			
		}catch(Exception e) {
			e.printStackTrace();
			lastException=e.getMessage();
			return ERR_SQLSYNTAX;
		}
		return 0;
	}
	private int parsePkey(PKey pkeyAnnotation,SqlEntity entry,SqlColumn col,
			SqlQueryBuilder qb, SqlTypeParser tp, SqlDataRegister tmp) {
		try {
			SqlConstraint c=new SqlConstraint();
			c.setConstraintName(pkeyAnnotation.name().length()==0?
					               "pk_"+entry.getTable():
					               pkeyAnnotation.name());
			c.setConstraintType(SqlConstraintType.pkey);
			c.addColumn(col);
			if(pkeyAnnotation.auto())c.addParam(SqlConstraintParam.PKEY_AUTO, true);
			entry.addConstraint(c);		
		}catch(Exception e) {
			e.printStackTrace();
			lastException=e.getMessage();
			return ERR_SQLSYNTAX;
		}
		return 0;		
	}
	private int parseFkey(FKey fkeyAnnotation,SqlEntity entry,SqlColumn col,
			SqlQueryBuilder qb, SqlTypeParser tp, SqlDataRegister tmp) {
		try {
			int regRef=register(fkeyAnnotation.references(),qb,tp,tmp);
			if(regRef>ERR_ENTITYREGISTERED)return regRef;
			SqlEntity ref=this.get(fkeyAnnotation.references());
			if(ref==null)ref=tmp.get(fkeyAnnotation.references());
			if(ref.getPrimaryKey()==null)throw new ConstraintViolationException("refered "+ref.getTable()+" has no primary key");
			SqlConstraint c=new SqlConstraint();
			c.setConstraintName(fkeyAnnotation.name().length()==0?
					               "fk_"+entry.getTable()+
					                   "_"+col.getColName()+
					                   "_"+ref.getTable():
					               fkeyAnnotation.name());
			c.setConstraintType(SqlConstraintType.fkey);
			c.addColumn(col);
			c.addParam(SqlConstraintParam.FKEY_REF, ref);
			c.addParam(SqlConstraintParam.FKEY_UPD, fkeyAnnotation.update());
			c.addParam(SqlConstraintParam.FKEY_DEL, fkeyAnnotation.delete());
			entry.addConstraint(c);	
		}catch(Exception e) {
			e.printStackTrace();
			lastException=e.getMessage();
			return ERR_SQLSYNTAX;
		}
		return 0;		
	}
	
	public LinkedList<SqlEntity> sync(SqlDataRegister source,boolean override) {
		LinkedList<SqlEntity>newEntities=new LinkedList<>();
		for(Entry<Class<?>, SqlEntity> entry : source.entityMapping.entrySet()) {
			if(this.entityMapping.containsKey(entry.getKey())&&!override)continue;
			this.entityMapping.put(entry.getKey(), entry.getValue());
			newEntities.add(entry.getValue());
		}
		return newEntities;
	}
	
	public boolean   contains(Class<?>entity) {
		return entityMapping.containsKey(entity);
	}
	
	public SqlEntity get(Class<?>entity) {
		return entityMapping.get(entity);
	}
}
