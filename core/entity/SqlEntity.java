package pollub.iist.sem6.ism.lab3a.util.orm.core.entity;

import java.lang.reflect.Field;
import java.util.LinkedList;
import java.util.ListIterator;
import java.util.LinkedHashMap;
import java.util.Map.Entry;
import java.util.Set;

import pollub.iist.sem6.ism.lab3a.util.orm.core.exception.ColumnRedeclarationException;
import pollub.iist.sem6.ism.lab3a.util.orm.core.exception.ConstraintViolationException;
import pollub.iist.sem6.ism.lab3a.util.orm.core.exception.InvalidQueryException;
import pollub.iist.sem6.ism.lab3a.util.orm.query.FKeyReferentialAction;
import pollub.iist.sem6.ism.lab3a.util.orm.query.SqlColumnParam;
import pollub.iist.sem6.ism.lab3a.util.orm.query.SqlConstraintParam;
import pollub.iist.sem6.ism.lab3a.util.orm.query.SqlConstraintType;
import pollub.iist.sem6.ism.lab3a.util.orm.query.SqlObject;
import pollub.iist.sem6.ism.lab3a.util.orm.query.SqlQueryBuilder;

public class SqlEntity {
	private Class<?>entity; 
	private String  tableName;
	private LinkedHashMap<String,SqlColumn>    columns;
	private LinkedHashMap<String,SqlConstraint>constraints;
	private SqlConstraint   primaryKey;
	private SqlQueryBuilder queryBuilder;

	private boolean autoId=false;
	private Integer lastId=0;
	public Integer getAutoId(){
		if(!autoId)return null;
		return ++lastId;
	}

	public String getColumnName(Field f){
		if(f.getDeclaringClass()==entity)
			return getColumnName(f.getName());
		return null;
	}
	public String getColumnName(String field){
		for(Entry<String,SqlColumn>e : columns.entrySet())
			if(e.getValue().getFieldName()==field)
				return e.getKey();
		return null;
	}

	public SqlEntity(Class<?>entity,SqlQueryBuilder qb) {
		if(entity==null)throw new IllegalArgumentException("@entity class cannot be null");
		if(qb==null)throw new IllegalArgumentException("@queryBuilder cannot be null");
		this.entity=entity;
		this.queryBuilder=qb;
		columns=new LinkedHashMap<>();
		constraints=new LinkedHashMap<>();
		primaryKey=null;
	}

	public final SqlConstraint getPrimaryKey() {
		return this.primaryKey;
	}
	public final SqlConstraint getConstraint(String name) {
		return constraints.get(name);
	}
	public void addConstraint(SqlConstraint c) throws ConstraintViolationException {
		if(c==null)throw new IllegalArgumentException("null @constraint not allowed");
		if(c.getConstraintType()==SqlConstraintType.pkey) {
			if(primaryKey==null)primaryKey=c;
			else if(primaryKey!=c&&
			        !primaryKey.getConstraintName().contentEquals(c.getConstraintName()))
				throw new ConstraintViolationException("primary key redeclared");
		}
		SqlConstraint cc=constraints.get(c.getConstraintName());
		if(cc==null)constraints.put(c.getConstraintName(), c);
		else if(cc==c)return;
		else {
			if(cc.getConstraintType()!=c.getConstraintType())
				throw new ConstraintViolationException("constraint name: "+c.getConstraintName()+" redeclared as: "+
			                                           c.getConstraintType().toString()+", previously declared as: "+
						                               cc.getConstraintType());
			switch(c.getConstraintType()) {
			case check:
				break;
			case defval:
				break;
			case fkey:
				if(cc.getParam(SqlConstraintParam.FKEY_REF)!=c.getParam(SqlConstraintParam.FKEY_REF))
					throw new ConstraintViolationException(c.getConstraintName()+" can only refer to one entity");
				break;
			case unique:
				break;
			}
			cc.addColumns(c.getColumns());
		}
	}
	
	public final SqlColumn getColumn(String name) {
		return columns.get(name);
	}
	public void addColumn(SqlColumn c) throws ColumnRedeclarationException {
		SqlColumn cc=columns.get(c.getColName());
		if(cc!=null&&cc!=c)
			throw new ColumnRedeclarationException(tableName+"."+c.getColName()+" already declared on "+cc.getFieldName());
		columns.put(c.getColName(), c);
	}

	public final String[] getColumnNames(){
		Set<String> keySet=columns.keySet();
		String[]out=new String[keySet.size()];
		int i=0;
		for(String key : keySet)out[i++]=key;
		return out;
	}
	
	public final String getTable() {
		return this.tableName;
	}
	public void setTable(String table) {
		this.tableName=table;
	}
	
	public LinkedHashMap<String,String> pojoToSqlDict(Object o){
		if(o.getClass()!=entity)throw new IllegalArgumentException("@argument class doesn't match mapping");
		LinkedHashMap<String,String>res=new LinkedHashMap<>();
		for(SqlColumn col : columns.values()) {
			try {
				Field f=entity.getDeclaredField(col.getFieldName());
				f.setAccessible(true);
				String strVal=col.getValueParser().fromValue(f.get(o));
				if(strVal==null&&!col.hasModifier(SqlColumnParam.notNull))
					throw new ConstraintViolationException(tableName+"."+col.getColName()+" declared as NOT NULL");
				res.put(col.getColName(), strVal);
			}catch(Exception e) {
				e.printStackTrace();
			}
		}
		return res;
	}
	
	public void sqlDictToPojo(Object o,LinkedHashMap<String,String>values) {
		if(o.getClass()!=entity)throw new IllegalArgumentException("@argument class doesn't match mapping");
		for(Entry<String,String>entry : values.entrySet()) {
			try {
				SqlColumn col=columns.get(entry.getKey());
				if(col==null)continue;
				Field f=entity.getDeclaredField(col.getFieldName());
				f.setAccessible(true);
				f.set(o,col.getValueParser().fromString(entry.getValue()));
			}catch(Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public LinkedHashMap<String,Object> sqlDictToPojoDict(LinkedHashMap<String,String>dict){
		LinkedHashMap<String,Object>res=new LinkedHashMap<>();
		for(Entry<String,String>entry : dict.entrySet()) {
			SqlColumn col=columns.get(entry.getKey());
			if(col==null)continue;
			res.put(col.getFieldName(), col.getValueParser().fromString(entry.getValue()));
		}
		return res;
	}

	public String getCreateQuery() {
		try {
			queryBuilder.open()                             // #begin
			            .create(SqlObject.table, tableName)	// CREATE TABLE IF NOT EXISTS @tableName
			            .open();                            // (

			int i=0;
			for(SqlColumn col : columns.values()) {
				if(i++>0)queryBuilder.comma();
				queryBuilder.column(col.getColName(),col.getSqlType(),col.getModifierString());
			}
			//if(!queryBuilder.handles(SqlObject.constraint)) {
				for(SqlConstraint constr : constraints.values()) {
					queryBuilder.comma()
								.constraint(constr.getConstraintName(), 
							                constr.getConstraintType().toString(),
							                constr.getColumnNames(),  
							                constr.getSqlParams());
				}
				queryBuilder.close();                           // )
			//} 
			queryBuilder.close();                           // #end
			return queryBuilder.toString();
		} catch (InvalidQueryException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public String getDropQuery() {
		try {
			queryBuilder.open()
			            .drop(SqlObject.table, tableName)
			            .close();
			return queryBuilder.toString();
		} catch (InvalidQueryException e) {
			e.printStackTrace();
			return null;
		}
	}

	public void optimize() throws ConstraintViolationException {
		if(primaryKey!=null) {
			Boolean auto_increment=(Boolean) primaryKey.getParam(SqlConstraintParam.PKEY_AUTO);
			LinkedList<SqlColumn>columns=primaryKey.getColumns();
			if(columns.size()==1&&auto_increment==Boolean.TRUE) {
				SqlColumn col=columns.get(0);
				this.autoId=true;
				if(queryBuilder.handles(SqlColumnParam.auto))
					if(col.getJavaType()==Integer.class||col.getJavaType()==int.class   ||
					   col.getJavaType()==Double.class ||col.getJavaType()==double.class||
					   col.getJavaType()==Float.class  ||col.getJavaType()==float.class) {
						constraints.remove(primaryKey.getConstraintName());
						col.addModifier(SqlColumnParam.pkey);
						col.addModifier(SqlColumnParam.auto);
					}
			}
			primaryKey.removeParam(SqlConstraintParam.PKEY_AUTO);
		}
		for(SqlConstraint constr : constraints.values()) {
			switch(constr.getConstraintType()) {
			case check:
				break;
			case defval:
				break;
			case fkey:
				SqlEntity ref=(SqlEntity)constr.getParam(SqlConstraintParam.FKEY_REF);
				LinkedList<SqlColumn>srcCols=constr.getColumns(),
				                     trgCols=ref.primaryKey.getColumns();
				if(srcCols.size()!=trgCols.size())
					throw new ConstraintViolationException(constr.getConstraintName()+" column set too small");
				ListIterator<SqlColumn> srcItr=srcCols.listIterator(),
						                trgItr=trgCols.listIterator();
				while(srcItr.hasNext()) {
					SqlColumn srcCol=srcItr.next(),trgCol=trgItr.next();
					if(!srcCol.getSqlType().contentEquals(trgCol.getSqlType()))
						throw new ConstraintViolationException(this.tableName+"."+srcCol.getColName()+" type: "+srcCol.getSqlType()+
								                               " doesn't match "+
					                                           ref.tableName +"."+trgCol.getColName()+": "+trgCol.getSqlType());
				}
				if(constr.getParam(SqlConstraintParam.FKEY_UPD)==FKeyReferentialAction.noAction)
					constr.removeParam(SqlConstraintParam.FKEY_UPD);
				if(constr.getParam(SqlConstraintParam.FKEY_DEL)==FKeyReferentialAction.noAction)
					constr.removeParam(SqlConstraintParam.FKEY_DEL);
				break;
			case pkey:
				break;
			case unique:
				break;
			}
		}
	}
}
