package pollub.iist.sem6.ism.lab3a.util.orm.core.entity;

import java.util.LinkedList;
import java.util.function.Function;
import java.util.stream.Collectors;

import pollub.iist.sem6.ism.lab3a.util.orm.query.SqlColumnParam;
import pollub.iist.sem6.ism.lab3a.util.orm.types.SqlValueParser;

public class SqlColumn {
	private String  columnName,
	                fieldName,
	                sqlType;
	private Class<?>javaType;
	
	private LinkedList<SqlColumnParam>modifiers=new LinkedList<>();
	private SqlValueParser valueParser;
	
	public final String   getColName() {
		return columnName;
	}
	public final String   getFieldName() {
		return fieldName;
	}
	public final String   getSqlType() {
		return sqlType;
	}	
	public final Class<?> getJavaType() {
		return javaType;
	}
	public final boolean hasModifier(SqlColumnParam mod) {
		return modifiers.contains(mod);
	}
	public final LinkedList<SqlColumnParam> getModifiers() {
		return modifiers;
	}
	public final String getModifierString() {
		return modifiers.stream().map(new Function<SqlColumnParam,String>(){
			@Override
			public String apply(SqlColumnParam arg0) {
				return arg0.toString();
			}
		}).collect(Collectors.joining(" "));
	}
	public final SqlValueParser getValueParser() {
		return valueParser;
	}
	
	public void setColName(String name) {
		columnName=name;
	}
	public void setFieldName(String name) {
		fieldName=name;
	}
	public void setSqlType(String type) {
		sqlType=type;
	}
	public void setJavaType(Class<?>type) {
		javaType=type;
	}
	public void addModifier(SqlColumnParam mod) {
		if(!modifiers.contains(mod))modifiers.add(mod);
	}
	public void removeModifier(SqlColumnParam mod) {
		modifiers.remove(mod);
	}
	public void setValueParser(SqlValueParser parser) {
		valueParser=parser;
	}
}
