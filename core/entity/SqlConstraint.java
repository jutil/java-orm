package pollub.iist.sem6.ism.lab3a.util.orm.core.entity;

import java.util.LinkedList;
import java.util.List;
import java.util.LinkedHashMap;
import java.util.Map.Entry;
import java.util.function.Function;
import java.util.stream.Collectors;

import pollub.iist.sem6.ism.lab3a.util.orm.query.SqlColumnParam;
import pollub.iist.sem6.ism.lab3a.util.orm.query.SqlConstraintParam;
import pollub.iist.sem6.ism.lab3a.util.orm.query.SqlConstraintType;

public class SqlConstraint {
	
	private SqlConstraintType constraintType=null;
	private String            constraintName;
	private LinkedList<SqlColumn>  columns=new LinkedList<>();
	private LinkedHashMap<SqlConstraintParam,Object>params =new LinkedHashMap<>();
	
	public final SqlConstraintType getConstraintType() {
		return constraintType;
	}
	public final String getConstraintName() {
		return constraintName;
	}
	public final LinkedList<SqlColumn> getColumns() {
		return columns;
	}
	public final LinkedHashMap<SqlConstraintParam,Object> getParams(){
		return params;
	}
	
	public void setConstraintType(SqlConstraintType type) {
		constraintType=type;
	}
	public void setConstraintName(String name) {
		constraintName=name;
	}
	public void addColumn(SqlColumn column) {
		for(SqlColumn col : columns)
			if(col.getColName()==column.getColName())
				return;
		columns.add(column);
		if(constraintType==SqlConstraintType.pkey)
			column.addModifier(SqlColumnParam.notNull);
	}
	public void addColumns(SqlColumn...columns) {
		for(SqlColumn c : columns)
			addColumn(c);
	}
	public void addColumns(List<SqlColumn>columns) {
		for(SqlColumn c : columns)
			addColumn(c);
	}
	public void addParam(SqlConstraintParam paramName,Object paramArg) {
		if(paramName.isViable(this.constraintType))
			params.put(paramName, paramArg);
	}
	
	public boolean contains(String colName) {
		for(SqlColumn col : columns)
			if(col.getColName()==colName)
				return true;
		return false;
	}
	
	public int getSize() {
		return this.columns.size();
	}
	public String getColumnNames() {
		return "("+columns.stream().map(new Function<SqlColumn,String>(){
			@Override
			public String apply(SqlColumn arg0) {
				return arg0.getColName();
			}
		}).collect(Collectors.joining(","))+")";
	}
	public Object getParam(SqlConstraintParam param) {
		return params.get(param);
	}
	public void removeParam(SqlConstraintParam param) {
		params.remove(param);
	}
	public String[] getSqlParams() {
		String[]out=new String[params.size()];
		int i=0;
		for(Entry<SqlConstraintParam, Object> param : params.entrySet()) {
			StringBuilder sb=new StringBuilder();
			sb.append(param.getKey().toString()).append(' ');
			switch(param.getKey()) {
			case CHK_COND:
				sb.append('(')
				  .append(param.getValue())
				  .append(')');
				break;
			case PKEY_AUTO:
				
				break;
			case FKEY_REF:
				SqlEntity ref=(SqlEntity)param.getValue();
				//String[] colNames=ref.getPrimaryKey().getColumnNames();
				sb.append(ref.getTable());
				sb.append(ref.getPrimaryKey().getColumnNames());
				/*sb.append('(');
				int j=0;
				sb.append(colNames[j++]);
				while(j<colNames.length)
					sb.append(',')
					  .append(colNames[j++]);
				sb.append(')');*/
				break;
			case FKEY_UPD:
			case FKEY_DEL:
				sb.append(param.getValue().toString());
				break;
			}
			out[i++]=sb.toString();
		}
		return out;
	}
}
