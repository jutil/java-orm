package pollub.iist.sem6.ism.lab3a.util.orm.core.exception;

public class ConstraintViolationException extends OrmException {

	public ConstraintViolationException(String string) {
		super(string);
	}

	private static final long serialVersionUID = 3308458123268456379L;

}
