package pollub.iist.sem6.ism.lab3a.util.orm.core.exception;

public class OrmException extends Exception {

	public OrmException() {}
	public OrmException(String string) {
		super(string);
	}

	private static final long serialVersionUID = 381410201498858823L;

}
