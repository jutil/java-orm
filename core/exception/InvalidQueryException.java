package pollub.iist.sem6.ism.lab3a.util.orm.core.exception;

public class InvalidQueryException extends OrmException {

	public InvalidQueryException(String string) {
		super(string);
	}

	private static final long serialVersionUID = 2581222431247043364L;

}
