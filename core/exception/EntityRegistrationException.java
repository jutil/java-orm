package pollub.iist.sem6.ism.lab3a.util.orm.core.exception;

public class EntityRegistrationException extends OrmException {
	protected int errCode;
	public EntityRegistrationException(int errCode) {
		this.errCode=errCode;
	}
	public EntityRegistrationException(int errCode,String msg) {
		super(msg);
		this.errCode=errCode;
	}
	
	public final int getErrCode() {
		return errCode;
	}

	private static final long serialVersionUID = -8657440896792037834L;

}
