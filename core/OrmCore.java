package pollub.iist.sem6.ism.lab3a.util.orm.core;

import java.util.ArrayList;
import java.util.LinkedList;

import pollub.iist.sem6.ism.lab3a.util.orm.core.entity.SqlDataRegister;
import pollub.iist.sem6.ism.lab3a.util.orm.core.entity.SqlEntity;
import pollub.iist.sem6.ism.lab3a.util.orm.core.exception.EntityRegistrationException;
import pollub.iist.sem6.ism.lab3a.util.orm.core.exception.OrmException;
import pollub.iist.sem6.ism.lab3a.util.orm.core.provider.SqlRdbms;
import pollub.iist.sem6.ism.lab3a.util.orm.query.SqlQueryBuilderFactory;
import pollub.iist.sem6.ism.lab3a.util.orm.query.SqlQueryBuilderRegister;
import pollub.iist.sem6.ism.lab3a.util.orm.types.SqlTypeParser;
import pollub.iist.sem6.ism.lab3a.util.orm.types.SqlTypeParserRegister;
import pollub.iist.sem6.ism.lab3a.util.orm.core.provider.PredefinedSqlRdbms;
import pollub.iist.sem6.ism.lab3a.util.orm.core.provider.SqlProvider;

public class OrmCore {
	private final SqlRdbms        rdbms;
	private       SqlProvider     provider;
	private final SqlDataRegister dataRegister;
	private final SqlTypeParser   typeParser;
	
	private final SqlQueryBuilderFactory queryBuilderFactory;

	public OrmCore(PredefinedSqlRdbms rdbms,SqlProvider provider) {
		this(rdbms,provider,SqlQueryBuilderRegister.get(rdbms));
	}
	public OrmCore(SqlRdbms rdbms,SqlProvider provider,SqlQueryBuilderFactory qbf) {
		if(rdbms   ==null)throw new IllegalArgumentException("null @rdbms argument");
		if(provider==null)throw new IllegalArgumentException("null @provider argument");
		if(qbf     ==null)throw new IllegalArgumentException("null @querybuilderfactory argument");
		this.rdbms=rdbms;
		this.provider=provider;
		this.queryBuilderFactory=qbf;
		this.typeParser=SqlTypeParserRegister.get(rdbms);
		if(this.typeParser==null)
			throw new IllegalArgumentException("no type parser available for "+
		                                          rdbms.getClass().getName());
		this.dataRegister=new SqlDataRegister();
	}
	
	public void register(Class<?>entity) throws OrmException {
		SqlEntity sqlEntity=dataRegister.get(entity);
		if(sqlEntity!=null)provider.table(sqlEntity.getTable(),sqlEntity.getDropQuery());
		try {
			LinkedList<SqlEntity>newEntities=dataRegister.register(entity, queryBuilderFactory.create(), typeParser);
			for(SqlEntity e : newEntities)provider.table(e.getTable(),e.getCreateQuery());
		} catch (EntityRegistrationException e) {
			e.printStackTrace();
		}
	}

	public final String getTableName(Class<?>entity){
		SqlEntity e = dataRegister.get(entity);
		return e==null?null:e.getTable();
	}

	public final SqlEntity getSqlEntity(Class<?>entity){
		return dataRegister.get(entity);
	}
	
	public final SqlRdbms getRdbms() {
		return rdbms;
	}
	
	/*public void setProvider(SqlProvider provider) {
		this.provider=provider;
	}
	public final SqlProvider getProvider() {
		return provider;
	}*/
}
