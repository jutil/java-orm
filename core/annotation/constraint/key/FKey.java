package pollub.iist.sem6.ism.lab3a.util.orm.core.annotation.constraint.key;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import pollub.iist.sem6.ism.lab3a.util.orm.query.FKeyReferentialAction;

/// @FKey
// usage:
//   specifies table column as foreign key
// arguments:
//   @name         string      constraint name, essential for multi-column
//                             defaults to fk_@Entity.table_@Column.name_@references.table
//   @references   entity      foreign key target
//   @update       referential specifies ON UPDATE behaviour
//                             defaults to NO ACTION
//   @delete       referential specifies ON DELETE behaviour
//                             defaults to NO ACTION
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface FKey {
	String   name() default "";
	// target must be @Entity annotated
    Class<?> references();
    FKeyReferentialAction update() default FKeyReferentialAction.noAction;
    FKeyReferentialAction delete() default FKeyReferentialAction.noAction;
}
