package pollub.iist.sem6.ism.lab3a.util.orm.core.annotation.constraint.key;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/// @PKey
// usage:
//   specifies table column as primary key or part of primary key
// arguments:
//   @name   string    constraint name, essential for multi-column
//                     defaults to pk_@Entity.table
//   @auto   boolean   specifies auto-generation policy
//                     defaults to NO AUTO_INCREMENT
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface PKey {
	String  name() default "";
    boolean auto() default false;
}
