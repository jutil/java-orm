package pollub.iist.sem6.ism.lab3a.util.orm.core.annotation.constraint;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/// @Check
// usage:
//   adds check constraint (specified by @condition) on column
// arguments:
//   @name       string   constraint name, essential for multi-column
//                        defaults to chk_@Entity.table
//   @condition  string   validation condition
@Retention(RUNTIME)
@Target(FIELD)
public @interface Check {
	String name() default "";
	// must not violate type integrity
	String condition();
}
