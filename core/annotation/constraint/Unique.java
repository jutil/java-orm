package pollub.iist.sem6.ism.lab3a.util.orm.core.annotation.constraint;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/// @Unique
// usage:
//   specifies column as unique-value
// arguments:
//   @name    string    constraint name, essential for multi-column
//                      defaults to uq_@Entity.table_@Column.name
@Retention(RUNTIME)
@Target(FIELD)
public @interface Unique {
	String name() default "";
}
