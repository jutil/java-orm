package pollub.iist.sem6.ism.lab3a.util.orm.core.annotation.constraint;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/// @Default
// usage:
//   registers default value generator on a field
// arguments:
//   @name       string   constraint name, essential for multi-column
//                        defaults to def_@Entity.table_@Column.name
//   @provider   provider default value provider class
@Retention(RUNTIME)
@Target(FIELD)
public @interface Default {
    String   name() default "";

    Class<?> provider()
}
