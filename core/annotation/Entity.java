package pollub.iist.sem6.ism.lab3a.util.orm.core.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/// @Entity
// usage:
//   binds annotated class to database table
// arguments:
//   @table   string   table name
//                     defaults to Class.getSimpleName()
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface Entity {
    String table() default "";
}
