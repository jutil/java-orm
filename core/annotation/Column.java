package pollub.iist.sem6.ism.lab3a.util.orm.core.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/// @Column
// usage:
//   binds @Entity annotated class field to table column
// arguments:
//   @name     string   table column name
//                      defaults to Field.getName()
//   @type     string   table column type
//                      defaults to SqlTypeParser.parse(Field.getType())
//   @nullable boolean  table column nullability
//                      defaults to NULL
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Column {
    String  name()     default "";
    String  type()     default "";
    //String  defaults() default ""; - won't work in case of actual 0-length str as default value
    boolean nullable() default true;
}
