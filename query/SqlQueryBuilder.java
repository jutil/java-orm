package pollub.iist.sem6.ism.lab3a.util.orm.query;

import java.util.Stack;

import pollub.iist.sem6.ism.lab3a.util.orm.core.exception.InvalidQueryException;

public abstract class SqlQueryBuilder {
	
	protected static class InternalQueryStream{
		private StringBuilder sb;
		private String lastCommand;
		
		public InternalQueryStream() {
			this(null);
		}
		
		public InternalQueryStream(String lastCommand) {
			sb=new StringBuilder();
			this.lastCommand=lastCommand;
		}

		public InternalQueryStream append(String data) {
			sb.append(' ').append(data);
			return this;
		}
		
		public InternalQueryStream append(String[]sequence) {
			int idx=0;
			sb.append(sequence[idx++]);
			while(idx<sequence.length)
				sb.append(',').append(sequence[idx++]);
			return this;
		}
		
		public InternalQueryStream storeAppend(String data) {
			lastCommand=data;
			return append(data);
		}
		public String getLastCommand() { 
			return lastCommand; 
		}
		@Override
		public String toString() {
			return sb.toString();
		}
	}
	
	protected Stack<InternalQueryStream>openQueries;
	protected String                    lastQuery;
	
	protected int handledObjects,
	              handledConstraints,
	              handledColumnParams;
	protected SqlQueryBuilder() {
		openQueries=new Stack<>();
		handledObjects=SqlObject.schema.mask    |
				       SqlObject.table.mask     |
				       SqlObject.trigger.mask   |
				       SqlObject.constraint.mask|
				       SqlObject.index.mask;
		handledConstraints=SqlConstraintType.unique.mask|
				           SqlConstraintType.check.mask |
				           SqlConstraintType.defval.mask|
				           SqlConstraintType.fkey.mask  |
				           SqlConstraintType.pkey.mask;
		handledColumnParams=SqlColumnParam.unique.mask |
				            SqlColumnParam.notNull.mask|
				            SqlColumnParam.auto.mask;
	}
	
	public boolean         isOpen() {
		return openQueries.size()>0;
	}
	
	public SqlQueryBuilder open() {
		InternalQueryStream iqs;
		if(isOpen())iqs=new InternalQueryStream(openQueries.peek().getLastCommand());
		else iqs=new InternalQueryStream();
		openQueries.push(iqs);
		return this;
	}
	
	public SqlQueryBuilder close() throws InvalidQueryException {
		if(openQueries.size()==0)throw new InvalidQueryException("no open query");
		InternalQueryStream iqs=openQueries.pop();
		if(openQueries.size()==0)lastQuery=iqs.toString();
		else openQueries.peek().append("("+iqs.toString()+")");
		return this;
	}
	
	protected InternalQueryStream currStream() {
		if(!isOpen())return null;
		return openQueries.peek();
	}
	
	/// LIFECYCLE MANAGEMENT ///
	public abstract SqlQueryBuilder create(SqlObject obj,String name) throws InvalidQueryException;
	public abstract SqlQueryBuilder drop  (SqlObject obj,String name) throws InvalidQueryException;
	public abstract SqlQueryBuilder alter (SqlObject obj,String name) throws InvalidQueryException;
	
	public abstract SqlQueryBuilder column(String name,String type,String modifiers) throws InvalidQueryException;
	public abstract SqlQueryBuilder constraint(String name,String type,String string,String[]modifiers
			) throws InvalidQueryException;
	
	
	/// SELECT QUERY ///
	
	
	public boolean handles(SqlObject obj) {
		return (obj.mask&handledObjects)!=0;
	}
	
	public boolean handles(SqlConstraintType constr) {
		return (constr.mask&handledConstraints)!=0;
	}
	
	public boolean handles(SqlColumnParam param) {
		return (param.mask&handledColumnParams)!=0;
	}
	
	@Override
	public String toString() {
		return lastQuery;
	}

	public SqlQueryBuilder comma() {
		currStream().sb.append(',');
		return this;
	}
}
