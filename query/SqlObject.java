package pollub.iist.sem6.ism.lab3a.util.orm.query;

public enum SqlObject {
	schema(0,"SCHEMA"),table(1,"TABLE"),
	constraint(2,"CONSTRAINT"),trigger(3,"TRIGGER"),
	index(4,"INDEX"),domain(5,"DOMAIN");
	private final String sql;
	public final int    mask;
	private SqlObject(int i,String s) {
		mask=1<<i;
		sql=s;
	}	
	@Override
	public String toString() {
		return sql;
	}
}
