package pollub.iist.sem6.ism.lab3a.util.orm.query;

public enum SqlConstraintType {
	unique(0,"UNIQUE"),check(1,"CHECK"),
	defval(2,"DEFAULT"),pkey(3,"PRIMARY KEY"),
	fkey  (4,"FOREIGN KEY");
	private final String sql;
	public final int    mask;
	private SqlConstraintType(int i,String s) {
		mask=1<<i;
		sql=s;
	}	
	@Override
	public String toString() {
		return sql;
	}
}
