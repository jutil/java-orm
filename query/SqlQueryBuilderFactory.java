package pollub.iist.sem6.ism.lab3a.util.orm.query;

public interface SqlQueryBuilderFactory {
	public SqlQueryBuilder create();
}
