package pollub.iist.sem6.ism.lab3a.util.orm.query.predefined;

import pollub.iist.sem6.ism.lab3a.util.orm.core.exception.InvalidQueryException;
import pollub.iist.sem6.ism.lab3a.util.orm.core.provider.PredefinedSqlRdbms;
import pollub.iist.sem6.ism.lab3a.util.orm.core.provider.SqlRdbms;
import pollub.iist.sem6.ism.lab3a.util.orm.query.SqlColumnParam;
import pollub.iist.sem6.ism.lab3a.util.orm.query.SqlObject;
import pollub.iist.sem6.ism.lab3a.util.orm.query.SqlQueryBuilder;
import pollub.iist.sem6.ism.lab3a.util.orm.query.SqlQueryBuilderFactory;

public class OracleSqlQueryBuilder extends SqlQueryBuilder {
	
	public static final class OracleSqlQueryBuilderFactory implements SqlQueryBuilderFactory {
		@Override
		public SqlQueryBuilder create() {
			return new OracleSqlQueryBuilder();
		}
	}
	
	@Override
	public SqlQueryBuilder create(SqlObject obj, String name) throws InvalidQueryException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SqlQueryBuilder drop(SqlObject obj, String name) throws InvalidQueryException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SqlQueryBuilder alter(SqlObject obj, String name) throws InvalidQueryException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SqlQueryBuilder column(String name, String type, String modifiers) throws InvalidQueryException {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public SqlQueryBuilder constraint(String name, String type, String columns, String[] modifiers)
			throws InvalidQueryException {
		// TODO Auto-generated method stub
		return null;
	}

}
