package pollub.iist.sem6.ism.lab3a.util.orm.query.predefined;

import pollub.iist.sem6.ism.lab3a.util.orm.core.exception.InvalidQueryException;
import pollub.iist.sem6.ism.lab3a.util.orm.query.SqlColumnParam;
import pollub.iist.sem6.ism.lab3a.util.orm.query.SqlObject;
import pollub.iist.sem6.ism.lab3a.util.orm.query.SqlQueryBuilder;
import pollub.iist.sem6.ism.lab3a.util.orm.query.SqlQueryBuilderFactory;

public class SqLiteQueryBuilder extends SqlQueryBuilder {
	
	public static final class SqLiteQueryBuilderFactory implements SqlQueryBuilderFactory {
		@Override
		public SqlQueryBuilder create() {
			return new SqLiteQueryBuilder();
		}
	}
	
	public SqLiteQueryBuilder() {
		super();
		handledObjects^=SqlObject.constraint.mask;
		handledObjects|=SqlColumnParam.pkey.mask;
	}
	
	@Override
	public SqlQueryBuilder create(SqlObject obj, String name) throws InvalidQueryException {
		if(!this.handles(obj))throw new InvalidQueryException(obj.name()+" not handled");
		currStream().storeAppend("CREATE "+obj.toString())
		            .append("IF NOT EXISTS")
		            .append(name);
		return this;
	}

	@Override
	public SqlQueryBuilder drop(SqlObject obj, String name) throws InvalidQueryException {
		if(!this.handles(obj))throw new InvalidQueryException(obj.name()+" not handled");
		currStream().storeAppend("DROP "+obj.toString())
		            .append("IF EXISTS")
		            .append(name);
		return this;
	}

	@Override
	public SqlQueryBuilder alter(SqlObject obj, String name) throws InvalidQueryException {
		if(!this.handles(obj))throw new InvalidQueryException(obj.name()+" not handled");
		currStream().storeAppend("ALTER "+obj.toString());
		return this;
	}

	@Override
	public SqlQueryBuilder column(String name, String type, String modifiers) throws InvalidQueryException {
		if(!currStream().getLastCommand().contentEquals("CREATE TABLE"))
			throw new InvalidQueryException("no table specified");
		currStream().append(name).append(type).append(modifiers);
		return this;
	}

	@Override
	public SqlQueryBuilder constraint(String name, String type, String columns, String[]modifiers)
			throws InvalidQueryException {
		if(!currStream().getLastCommand().contentEquals("CREATE TABLE"))
			throw new InvalidQueryException("no table specified");
		currStream().append(type).append(columns);
		for(String mod : modifiers)
			if(mod!=null)
				currStream().append(mod);
		return this;
	}

}
