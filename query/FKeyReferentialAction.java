package pollub.iist.sem6.ism.lab3a.util.orm.query;

public enum FKeyReferentialAction {
	noAction("NO ACTION"),
	cascade("CASCADE"),
	setNull("SET NULL"),
	setDefault("SET DEFAULT");
	private final String sql;
	private FKeyReferentialAction(String sql) {
		this.sql=sql;
	}
	@Override
	public String toString() {
		return sql;
	}
}
