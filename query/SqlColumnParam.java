package pollub.iist.sem6.ism.lab3a.util.orm.query;

public enum SqlColumnParam {
	unique(0,"UNIQUE"),
	notNull(2,"NOT NULL"),auto(3,"AUTO_INCREMENT"),
	pkey(4,"PRIMARY KEY");
	private final String sql;
	public final int    mask;
	private SqlColumnParam(int i,String s) {
		mask=1<<i;
		sql=s;
	}	
	@Override
	public String toString() {
		return sql;
	}
}
