package pollub.iist.sem6.ism.lab3a.util.orm.query;

import java.util.HashMap;

import pollub.iist.sem6.ism.lab3a.util.orm.core.provider.PredefinedSqlRdbms;
import pollub.iist.sem6.ism.lab3a.util.orm.core.provider.SqlRdbms;
import pollub.iist.sem6.ism.lab3a.util.orm.query.predefined.*;

public class SqlQueryBuilderRegister {
	
	private static final HashMap<SqlRdbms,SqlQueryBuilderFactory> factoryMapping=new HashMap<>();
	static {
		factoryMapping.put(PredefinedSqlRdbms.MSSQL,     new MsSqlQueryBuilder.MsSqlQueryBuilderFactory());
		factoryMapping.put(PredefinedSqlRdbms.MySQL,     new MySqlQueryBuilder.MySqlQueryBuilderFactory());
		factoryMapping.put(PredefinedSqlRdbms.Oracle,    new OracleSqlQueryBuilder.OracleSqlQueryBuilderFactory());
		factoryMapping.put(PredefinedSqlRdbms.PostgreSQL,new PostgreSqlQueryBuilder.PostgreSqlQueryBuilderFactory());
		factoryMapping.put(PredefinedSqlRdbms.SQLite,    new SqLiteQueryBuilder.SqLiteQueryBuilderFactory());
	}
	
	public static void register(SqlRdbms rdbms,SqlQueryBuilderFactory factory) {
		register(rdbms,factory,false);
	}

	public static void register(SqlRdbms rdbms, SqlQueryBuilderFactory factory, boolean override) {
		if(factoryMapping.containsKey(rdbms)&&!override)
			throw new IllegalArgumentException("@rdbms already mapped");
		factoryMapping.put(rdbms, factory);
	}
	
	public static SqlQueryBuilderFactory get(SqlRdbms rdbms) {
		SqlQueryBuilderFactory qbf=factoryMapping.get(rdbms);
		if(qbf==null)throw new IllegalArgumentException("no/null mapping for @rdbms");
		return qbf;
	}
}
