package pollub.iist.sem6.ism.lab3a.util.orm.query;

import java.util.ArrayList;

public enum SqlConstraintParam {
	CHK_COND("",SqlConstraintType.check),
	PKEY_AUTO("AUTO_INCREMENT",SqlConstraintType.pkey),
	FKEY_REF("REFERENCES",SqlConstraintType.fkey),FKEY_UPD("ON UPDATE",SqlConstraintType.fkey),FKEY_DEL("ON DELETE",SqlConstraintType.fkey);
	private final String sql;
	private final ArrayList<SqlConstraintType> viable;
	private SqlConstraintParam(String sql,SqlConstraintType...viable) { 
		this.sql=sql;
		this.viable=new ArrayList<>();
		for(SqlConstraintType ct : viable)
			this.viable.add(ct);
	}
	public boolean isViable(SqlConstraintType ct) { return viable.contains(ct); }
	@Override
	public String toString() {
		return sql;
	}
}
